import json
import pymysql.cursors
import string

connection = pymysql.connect(host='localhost',
							 user='rob',
							 password='woohoo',
							 db='twitter',
							 charset='utf8mb4'
							 )

def save_tweet_to_db(tweet_json, is_retweet=False):

	tweet_fields = {}

	tweet_dict = json.loads(clean_text(tweet_json))

	if 'retweeted_status' in tweet_dict:
		tweet_fields['retweet_id'] = tweet_dict['retweeted_status']['id_str']

		original_tweet = json.dumps(tweet_dict['retweeted_status'])
		save_tweet_to_db(original_tweet, True)

	if 'quoted_status' in tweet_dict:
		tweet_fields['quoted_status_id_str'] = tweet_dict['quoted_status_id_str']

		original_tweet = json.dumps(tweet_dict['quoted_status'])
		save_tweet_to_db(original_tweet, True)

	user = tweet_dict['user']

	tweet_fields['user_created_at'] = clean_text(user['created_at'])
	tweet_fields['user_default_profile'] = boolean_to_string(user['default_profile'])
	tweet_fields['user_description'] = clean_text(user['description'])	
	tweet_fields['user_favourites_count'] = str(user['favourites_count'])
	tweet_fields['user_followers_count'] = str(user['followers_count'])
	tweet_fields['user_friends_count'] = str(user['friends_count'])
	tweet_fields['user_geo_enabled'] = boolean_to_string(user['geo_enabled'])
	tweet_fields['user_id_str'] = clean_text(user['id_str'])
	tweet_fields['user_listed_count'] = str(user['listed_count'])
	tweet_fields['user_location'] = clean_text(user['location'])
	tweet_fields['user_name'] = clean_text(user['name'])
	tweet_fields['user_protected'] = boolean_to_string(user['protected'])
	tweet_fields['user_screen_name'] = clean_text(user['screen_name'])
	tweet_fields['user_statuses_count'] = str(user['statuses_count'])
	tweet_fields['user_time_zone'] = clean_text(user['time_zone'])
	tweet_fields['user_url'] = clean_text(user['url'])
	tweet_fields['user_verified'] = boolean_to_string(user['verified'])
	tweet_fields['coordinates'] = clean_text(tweet_dict['coordinates'])
	tweet_fields['created_at'] = clean_text(tweet_dict['created_at'])
	tweet_fields['favorite_count'] = str(tweet_dict['favorite_count'])
	tweet_fields['id_str'] = clean_text(tweet_dict['id_str'])
	tweet_fields['in_reply_to_screen_name'] = clean_text(tweet_dict['in_reply_to_screen_name'])
	tweet_fields['in_reply_to_status_id_str'] = clean_text(tweet_dict['in_reply_to_status_id_str'])
	tweet_fields['in_reply_to_user_id_str'] = clean_text(tweet_dict['in_reply_to_user_id_str'])
	tweet_fields['retweet_count'] = str(tweet_dict['retweet_count'])
	tweet_fields['tweet_text'] = clean_text(tweet_dict['text'])

	if 'entities' in tweet_dict:
		tweet_fields['entities'] = clean_text(tweet_dict['entities'])

	if 'entities' in user:
		tweet_fields['user_entities'] = clean_text(user['entities'])

	if 'retweet_id' in tweet_dict:
		tweet_fields['retweet_id'] = clean_text(tweet_dict['retweet_id'])

	if is_retweet:
		table_name = 'retweeted_tweets'
	else:
		table_name = 'tweets'

	place_holders = '%s, ' * (len(tweet_fields.values()) - 1) + "%s"
	column_names = ', '.join(tweet_fields.keys())
	column_values = list(tweet_fields.values())

	query = 'insert into %s (%s) values(%s)' % (table_name, column_names, place_holders)


	try:
		with connection.cursor() as cursor:
			query_text = cursor.mogrify(query, column_values)
			cursor.execute(query,  column_values)
			connection.commit()
	except Exception as e: 	
		print("Query text:" + query_text)
		error_code = e.args[0]

		if is_ignorable(error_code):
	 		print(e.args[1] + ": continuing execution with next record.")
		else:
			print("Error encountered: " + e.args)
			raise 



def clean_text(input_text):
	if input_text is None:
		return ""

	printable = set(string.printable)

	return_text = str(input_text)
	return_text = filter(lambda x: x in printable, return_text)

	return_text = ''.join(return_text)

	return_text = return_text.replace("'","")

	return(return_text.encode('latin-1', 'ignore').decode('utf-8'))
	

def boolean_to_string(input_string):
	if input_string:
		return "1"
	else:
		return "0"


def is_ignorable(error_code):
	return (error_code == 1062)
















