-- MySQL dump 10.13  Distrib 5.7.11, for osx10.9 (x86_64)
--
-- Host: localhost    Database: twitter
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `retweeted_tweets`
--

DROP TABLE IF EXISTS `retweeted_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retweeted_tweets` (
  `user_created_at` varchar(30) DEFAULT NULL,
  `user_default_profile` tinyint(1) DEFAULT NULL,
  `user_description` text,
  `user_entities` text,
  `user_favourites_count` int(11) DEFAULT NULL,
  `user_followers_count` int(11) DEFAULT NULL,
  `user_friends_count` int(11) DEFAULT NULL,
  `user_geo_enabled` tinyint(1) DEFAULT NULL,
  `user_id_str` varchar(100) DEFAULT NULL,
  `user_listed_count` int(11) DEFAULT NULL,
  `user_location` varchar(300) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_protected` tinyint(1) DEFAULT NULL,
  `user_screen_name` varchar(100) DEFAULT NULL,
  `user_statuses_count` int(11) DEFAULT NULL,
  `user_time_zone` varchar(100) DEFAULT NULL,
  `user_url` varchar(200) DEFAULT NULL,
  `user_verified` tinyint(1) DEFAULT NULL,
  `coordinates` varchar(200) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `entities` text,
  `favorite_count` int(11) DEFAULT NULL,
  `id_str` varchar(100) NOT NULL,
  `in_reply_to_screen_name` varchar(100) DEFAULT NULL,
  `in_reply_to_status_id_str` varchar(100) DEFAULT NULL,
  `in_reply_to_user_id_str` varchar(100) DEFAULT NULL,
  `quoted_status_id_str` varchar(100) DEFAULT NULL,
  `retweet_count` int(11) DEFAULT NULL,
  `retweet_id` varchar(100) DEFAULT NULL,
  `tweet_text` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tweets` (
  `user_created_at` varchar(30) DEFAULT NULL,
  `user_default_profile` tinyint(1) DEFAULT NULL,
  `user_description` text,
  `user_entities` text,
  `user_favourites_count` int(11) DEFAULT NULL,
  `user_followers_count` int(11) DEFAULT NULL,
  `user_friends_count` int(11) DEFAULT NULL,
  `user_geo_enabled` tinyint(1) DEFAULT NULL,
  `user_id_str` varchar(100) DEFAULT NULL,
  `user_listed_count` int(11) DEFAULT NULL,
  `user_location` varchar(300) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_protected` tinyint(1) DEFAULT NULL,
  `user_screen_name` varchar(100) DEFAULT NULL,
  `user_statuses_count` int(11) DEFAULT NULL,
  `user_time_zone` varchar(100) DEFAULT NULL,
  `user_url` varchar(200) DEFAULT NULL,
  `user_verified` tinyint(1) DEFAULT NULL,
  `coordinates` varchar(200) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `entities` text,
  `favorite_count` int(11) DEFAULT NULL,
  `id_str` varchar(100) NOT NULL,
  `in_reply_to_screen_name` varchar(100) DEFAULT NULL,
  `in_reply_to_status_id_str` varchar(100) DEFAULT NULL,
  `in_reply_to_user_id_str` varchar(100) DEFAULT NULL,
  `quoted_status_id_str` varchar(100) DEFAULT NULL,
  `retweet_count` int(11) DEFAULT NULL,
  `retweet_id` varchar(100) DEFAULT NULL,
  `tweet_text` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_str`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-20  9:36:03
